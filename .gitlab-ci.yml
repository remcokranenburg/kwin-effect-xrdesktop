.templates_sha: &templates_sha 290b79e0e78eab67a83766f4e9691be554fc4afd

include:
  - project: 'freedesktop/ci-templates'
    ref: *templates_sha
    file: '/templates/arch.yml'
  - project: "freedesktop/ci-templates"
    ref: *templates_sha
    file: "/templates/ubuntu.yml"
  - project: "freedesktop/ci-templates"
    ref: *templates_sha
    file: "/templates/debian.yml"

variables:
  FDO_UPSTREAM_REPO: xrdesktop/kwin-effect-xrdesktop
  GULKAN_COMMIT: "d0a4e797c83cb66592845a5a22729474a7b4c403"
  GXR_COMMIT: "7aebf3f73c67411a280c7ad623de2e56ae7ad65e"
  XRDESKTOP_COMMIT: "b7c2db4caec413dd9b06a93eaaf8eb8bbc98460e"
  LIBINPUTSYNTH_COMMIT: "724b82aaad96ea1ac7df5d5e10b3f14756223b1d"
  OPENXR_SDK_TAG: "release-1.0.14"
  OPENVR_TAG: "v1.14.15" # 1.16.8 is broken without patches

stages:
  - container_prep
  - build_and_test


# "Base" job for installing Gulkan, OpenXR SDK and OpenVR
.xrdesktop.base-job.build_deps:
  stage: container_prep
  variables:
    FDO_DISTRIBUTION_EXEC: |
      mkdir deps && \
      cd deps && \
      git clone https://gitlab.freedesktop.org/xrdesktop/gulkan.git && \
      cd gulkan && \
      git checkout $GULKAN_COMMIT && \
      meson build -Dexamples=false -Dtests=false --prefix /usr && \
      ninja -C build install && \
      cd .. && \
      git clone --depth 1 --branch $OPENXR_SDK_TAG https://github.com/KhronosGroup/OpenXR-SDK-Source.git && \
      cd OpenXR-SDK-Source && \
      cmake . -G Ninja -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=OFF -DDYNAMIC_LOADER=ON && \
      ninja -C . install && \
      cd .. && \
      git clone --depth 1 --branch $OPENVR_TAG https://github.com/ValveSoftware/openvr.git && \
      cd openvr && \
      cmake . -G Ninja -DBUILD_SHARED=1 -DCMAKE_INSTALL_PREFIX=/usr/ && \
      ninja -C . install && \
      cd .. && \
      git clone https://gitlab.freedesktop.org/xrdesktop/gxr.git && \
      cd gxr && \
      git checkout $GXR_COMMIT && \
      meson build -Dexamples=false -Dtests=false --prefix /usr && \
      ninja -C build install && \
      cd .. && \
      git clone https://gitlab.freedesktop.org/xrdesktop/xrdesktop.git && \
      cd xrdesktop && \
      git checkout $XRDESKTOP_COMMIT && \
      meson build -Dexamples=false -Dtests=false --prefix /usr && \
      ninja -C build install && \
      cd .. && \
      git clone https://gitlab.freedesktop.org/xrdesktop/libinputsynth.git && \
      cd libinputsynth && \
      git checkout $LIBINPUTSYNTH_COMMIT && \
      meson build -Dexamples=false -Dtests=false --prefix /usr && \
      ninja -C build install


# "Base" job for a Meson build
.xrdesktop.base-job.build:
  stage: build_and_test
  script:
    - cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr -GNinja -Bbuild
    - ninja -C build install

# variables shared by container_prep and build jobs
.xrdesktop.variables.arch:rolling:
  variables:
    FDO_DISTRIBUTION_TAG: "2021-03-24.0"

.xrdesktop.variables.ubuntu:focal:
  variables:
    FDO_DISTRIBUTION_VERSION: "20.04"
    FDO_DISTRIBUTION_TAG: "2021-03-24.0"

.xrdesktop.variables.ubuntu:groovy:
  variables:
    FDO_DISTRIBUTION_VERSION: "20.10"
    FDO_DISTRIBUTION_TAG: "2021-03-24.0"

.xrdesktop.variables.ubuntu:hirsute:
  variables:
    FDO_DISTRIBUTION_VERSION: "21.04"
    FDO_DISTRIBUTION_TAG: "2021-05-19.1"

.xrdesktop.variables.debian:buster:
  variables:
    FDO_DISTRIBUTION_VERSION: "buster"
    FDO_DISTRIBUTION_TAG: "2021-05-19.1"

.xrdesktop.variables.debian:bullseye:
  variables:
    FDO_DISTRIBUTION_VERSION: "bullseye"
    FDO_DISTRIBUTION_TAG: "2021-05-19.1"

.xrdesktop.variables.debian:sid:
  variables:
    FDO_DISTRIBUTION_VERSION: "sid"
    FDO_DISTRIBUTION_TAG: "2021-05-19.1"


# === Archlinux ===

arch:container_prep:
  extends:
    - .xrdesktop.variables.arch:rolling
    - .fdo.container-build@arch # from ci-templates
    - .xrdesktop.base-job.build_deps
  stage: container_prep
  variables:
    FDO_DISTRIBUTION_PACKAGES: "pkgconf meson gdk-pixbuf2 vulkan-headers vulkan-icd-loader graphene cairo glslang glfw-x11 glew shaderc json-glib gcc clang git cmake gtk3 libffi pygobject-devel kwin extra-cmake-modules xdotool"

arch:meson:gcc:
  extends:
    - .xrdesktop.variables.arch:rolling
    - .fdo.distribution-image@arch # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++

arch:meson:clang:
  extends:
    - .xrdesktop.variables.arch:rolling
    - .fdo.distribution-image@arch # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: clang
    CXX: clang++


# === Ubuntu Focal ===

ubuntu:focal:container_prep:
  stage: container_prep
  extends:
    - .xrdesktop.variables.ubuntu:focal
    - .fdo.container-build@ubuntu # from ci-templates
    - .xrdesktop.base-job.build_deps
  variables:
    FDO_DISTRIBUTION_PACKAGES: "git gcc meson pkg-config libglib2.0-dev libgdk-pixbuf2.0-dev libvulkan-dev libgraphene-1.0-dev libcairo2-dev glslang-tools cmake libxxf86vm-dev libgl1-mesa-dev libvulkan-dev libx11-xcb-dev libxcb-dri2-0-dev libxcb-glx0-dev libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-randr0-dev libxrandr-dev libxxf86vm-dev mesa-common-dev libgtk-3-dev libjson-glib-dev ca-certificates python3-dev python-gi-dev libkf5configwidgets-dev libkf5globalaccel-dev libkf5i18n-dev libkf5service-dev libkf5xmlgui-dev kwin-dev qtdeclarative5-dev plasma-workspace-dev gettext libxdo-dev"

ubuntu:focal:gcc:
  extends:
    - .xrdesktop.variables.ubuntu:focal
    - .fdo.distribution-image@ubuntu # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++
  before_script:
   - export PYTHONPATH=$PYTHONPATH:/usr/lib/python3.8/site-packages/ # Add standard python package path for tests

# === Ubuntu Groovy ===

ubuntu:groovy:container_prep:
  stage: container_prep
  extends:
    - .xrdesktop.variables.ubuntu:groovy
    - .fdo.container-build@ubuntu # from ci-templates
    - .xrdesktop.base-job.build_deps
  variables:
    FDO_DISTRIBUTION_PACKAGES: "git gcc meson pkg-config libglib2.0-dev libgdk-pixbuf2.0-dev libvulkan-dev libgraphene-1.0-dev libcairo2-dev glslang-tools cmake libxxf86vm-dev libgl1-mesa-dev libvulkan-dev libx11-xcb-dev libxcb-dri2-0-dev libxcb-glx0-dev libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-randr0-dev libxrandr-dev libxxf86vm-dev mesa-common-dev libgtk-3-dev libjson-glib-dev ca-certificates python3-dev python-gi-dev libkf5configwidgets-dev libkf5globalaccel-dev libkf5i18n-dev libkf5service-dev libkf5xmlgui-dev kwin-dev qtdeclarative5-dev plasma-workspace-dev gettext libxdo-dev"

ubuntu:groovy:gcc:
  extends:
    - .xrdesktop.variables.ubuntu:groovy
    - .fdo.distribution-image@ubuntu # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++
  before_script:
   - export PYTHONPATH=$PYTHONPATH:/usr/lib/python3.8/site-packages/ # Add standard python package path for tests

# === Ubuntu Hirsute ===

ubuntu:hirsute:container_prep:
  stage: container_prep
  extends:
    - .xrdesktop.variables.ubuntu:hirsute
    - .fdo.container-build@ubuntu # from ci-templates
    - .xrdesktop.base-job.build_deps
  variables:
    FDO_DISTRIBUTION_PACKAGES: "git gcc meson pkg-config libglib2.0-dev libgdk-pixbuf2.0-dev libvulkan-dev libgraphene-1.0-dev libcairo2-dev glslang-tools cmake libxxf86vm-dev libgl1-mesa-dev libvulkan-dev libx11-xcb-dev libxcb-dri2-0-dev libxcb-glx0-dev libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-randr0-dev libxrandr-dev libxxf86vm-dev mesa-common-dev libgtk-3-dev libjson-glib-dev ca-certificates python3-dev python-gi-dev libkf5configwidgets-dev libkf5globalaccel-dev libkf5i18n-dev libkf5service-dev libkf5xmlgui-dev kwin-dev qtdeclarative5-dev plasma-workspace-dev gettext libxdo-dev"

ubuntu:hirsute:gcc:
  extends:
    - .xrdesktop.variables.ubuntu:hirsute
    - .fdo.distribution-image@ubuntu # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++


# === Debian buster ===

debian:buster:container_prep:
  stage: container_prep
  extends:
    - .xrdesktop.variables.debian:buster
    - .fdo.container-build@debian # from ci-templates
    - .xrdesktop.base-job.build_deps
  variables:
    FDO_DISTRIBUTION_PACKAGES: "git gcc meson pkg-config libglib2.0-dev libgdk-pixbuf2.0-dev libvulkan-dev libgraphene-1.0-dev libcairo2-dev glslang-tools cmake libxxf86vm-dev libgl1-mesa-dev libvulkan-dev libx11-xcb-dev libxcb-dri2-0-dev libxcb-glx0-dev libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-randr0-dev libxrandr-dev libxxf86vm-dev mesa-common-dev libgtk-3-dev libjson-glib-dev ca-certificates python3-dev python-gi-dev libkf5configwidgets-dev libkf5globalaccel-dev libkf5i18n-dev libkf5service-dev libkf5xmlgui-dev kwin-dev qtdeclarative5-dev plasma-workspace-dev gettext libxdo-dev"

debian:buster:gcc:
  extends:
    - .xrdesktop.variables.debian:buster
    - .fdo.distribution-image@debian # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++


# === Debian bullseye ===

debian:bullseye:container_prep:
  stage: container_prep
  extends:
    - .xrdesktop.variables.debian:bullseye
    - .fdo.container-build@debian # from ci-templates
    - .xrdesktop.base-job.build_deps
  variables:
    FDO_DISTRIBUTION_PACKAGES: "git gcc meson pkg-config libglib2.0-dev libgdk-pixbuf2.0-dev libvulkan-dev libgraphene-1.0-dev libcairo2-dev glslang-tools cmake libxxf86vm-dev libgl1-mesa-dev libvulkan-dev libx11-xcb-dev libxcb-dri2-0-dev libxcb-glx0-dev libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-randr0-dev libxrandr-dev libxxf86vm-dev mesa-common-dev libgtk-3-dev libjson-glib-dev ca-certificates python3-dev python-gi-dev libkf5configwidgets-dev libkf5globalaccel-dev libkf5i18n-dev libkf5service-dev libkf5xmlgui-dev kwin-dev qtdeclarative5-dev plasma-workspace-dev gettext libxdo-dev"

debian:bullseye:gcc:
  extends:
    - .xrdesktop.variables.debian:bullseye
    - .fdo.distribution-image@debian # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++


# === Debian sid ===

debian:sid:container_prep:
  stage: container_prep
  extends:
    - .xrdesktop.variables.debian:sid
    - .fdo.container-build@debian # from ci-templates
    - .xrdesktop.base-job.build_deps
  variables:
    FDO_DISTRIBUTION_PACKAGES: "git gcc meson pkg-config libglib2.0-dev libgdk-pixbuf2.0-dev libvulkan-dev libgraphene-1.0-dev libcairo2-dev glslang-tools cmake libxxf86vm-dev libgl1-mesa-dev libvulkan-dev libx11-xcb-dev libxcb-dri2-0-dev libxcb-glx0-dev libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-randr0-dev libxrandr-dev libxxf86vm-dev mesa-common-dev libgtk-3-dev libjson-glib-dev ca-certificates python3-dev python-gi-dev libkf5configwidgets-dev libkf5globalaccel-dev libkf5i18n-dev libkf5service-dev libkf5xmlgui-dev kwin-dev qtdeclarative5-dev plasma-workspace-dev gettext libxdo-dev"

debian:sid:gcc:
  extends:
    - .xrdesktop.variables.debian:sid
    - .fdo.distribution-image@debian # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++
